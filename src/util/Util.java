package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.VerticalAlignment;

public class Util {

	public static void main(String[] args) throws Exception {
		FileInputStream fis = new FileInputStream(new File("tabelaNormalizada.xls"));
		HSSFWorkbook workbook = new HSSFWorkbook(fis);
		HSSFSheet spreadsheet = workbook.getSheetAt(0);
		Map<Integer, Map<Integer, List<Double>>> mapaAnoMesDia = new HashMap<>();
		extrairDadosTabela(spreadsheet, mapaAnoMesDia);

		Map<Integer, List<Double>> mapaValoresAno = new HashMap<>();
		agruparTodosOsDiasDoAnoPorAno(mapaAnoMesDia, mapaValoresAno);

		HSSFSheet spreadsheet1 = workbook.createSheet("Dados Formatados");
		// Create row object
		int contador = 0;
		int coluna = 0;

		HSSFCellStyle style = workbook.createCellStyle();
		style.setWrapText(true);

		HSSFCellStyle cellHeaderStyle = workbook.createCellStyle();

		cellHeaderStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.SEA_GREEN.getIndex());
		cellHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellHeaderStyle.setWrapText(true);
		cellHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		
		
		HSSFCellStyle cellStyle = workbook.createCellStyle();

		cellStyle.setWrapText(true);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		HSSFRow linhaCabecalho = spreadsheet1.createRow(coluna);
		linhaCabecalho.getRowStyle();
		for (Entry<Integer, List<Double>> mapa : mapaValoresAno.entrySet()) {
			Integer ano = mapa.getKey();

			Cell cell = linhaCabecalho.createCell(contador++);
			cell.setCellValue(ano);
			cell.setCellStyle(cellHeaderStyle);

			List<Double> listaDiasMes = mapa.getValue();
			int qtdDiasAno = 1;
			for (Double valor : listaDiasMes) {
				HSSFRow row = null;
				if (coluna == 0) {
					row = spreadsheet1.createRow(qtdDiasAno++);
				} else {
					row = spreadsheet1.getRow(qtdDiasAno++);
					if (row == null)
						continue;
				}
				HSSFCell cellA1 = row.createCell(coluna);
				cellA1.setCellValue(valor);
				cellA1.setCellStyle(cellStyle);
			}
			coluna++;

		}

		FileOutputStream out = new FileOutputStream(new File("resolvido.xlsx"));
		workbook.write(out);
		out.close();

		workbook.close();
		fis.close();

		System.out.println("Processo Terminado");
	}

	private static void extrairDadosTabela(HSSFSheet spreadsheet,
			Map<Integer, Map<Integer, List<Double>>> mapaAnoMesDia) {
		for (int i = 1; i < spreadsheet.getPhysicalNumberOfRows(); i++) {

			HSSFRow row = spreadsheet.getRow(i);
			Integer mesReferencia = null;
			Integer anoReferencia = null;
			for (int c = 0; c < row.getPhysicalNumberOfCells(); c++) {
				Cell cell = row.getCell(c);
				if (c == 0) {
					mesReferencia = (int) cell.getNumericCellValue();
					continue;
				} else if (c == 1) {
					anoReferencia = (int) cell.getNumericCellValue();
					continue;
				} else if (c == 2) {

					continue;

				} else {

					if (cell == null || cell.getCellTypeEnum() == null) {
						continue;
					}
					switch (cell.getCellTypeEnum()) {

					case NUMERIC:
						if (mapaAnoMesDia.containsKey(anoReferencia)) {

							Map<Integer, List<Double>> mapaAnoLocal = mapaAnoMesDia.get(anoReferencia);
							List<Double> list = new ArrayList<>();
							if (mapaAnoLocal.containsKey(mesReferencia)) {
								list = mapaAnoLocal.get(mesReferencia);
							}

							list.add(cell.getNumericCellValue());
							mapaAnoLocal.put(mesReferencia, list);

							mapaAnoMesDia.put(anoReferencia, mapaAnoLocal);
						} else {

							Map<Integer, List<Double>> tmp = new HashMap<>();
							List<Double> list = new ArrayList<>();
							list.add(cell.getNumericCellValue());
							tmp.put(mesReferencia, list);
							mapaAnoMesDia.put(anoReferencia, tmp);

						}
						break;
					case STRING:
						break;
					default:
						break;
					}
				}
			}

		}
	}

	private static void agruparTodosOsDiasDoAnoPorAno(Map<Integer, Map<Integer, List<Double>>> mapaAnoMesDia,
			Map<Integer, List<Double>> mapaValoresAno) {
		for (Entry<Integer, Map<Integer, List<Double>>> mapa : mapaAnoMesDia.entrySet()) {
			Integer ano = mapa.getKey();
			Map<Integer, List<Double>> value = mapa.getValue();
			for (Entry<Integer, List<Double>> mapaMesDias : value.entrySet()) {
				List<Double> listaDiasMes = mapaMesDias.getValue();
				for (Double valor : listaDiasMes) {
					List<Double> valoresDiaAno = mapaValoresAno.get(ano);
					if (valoresDiaAno == null) {
						valoresDiaAno = new ArrayList<>();
					}
					valoresDiaAno.add(valor);
					mapaValoresAno.put(ano, valoresDiaAno);
				}

			}

		}
	}

}
