package excel.jxl;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.CellView;
import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Excel {

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) throws IOException, BiffException, Exception {
		/**
		 *
		 * Carrega a planilha
		 *
		 */
		Workbook workbook = Workbook.getWorkbook(new File("/home/alexandre/excel/tabelaNormalizada.xls"));

		WritableWorkbook copiaPlanilha = Workbook.createWorkbook(new File("/home/alexandre/excel/Resultado.xls"),
				workbook);
		/**
		 *
		 * Aqui Ã© feito o controle de qual aba do xls * serÃ¡ realiza a leitura
		 * dos dados
		 *
		 */
		WritableSheet sheet = copiaPlanilha.getSheet(0);

		/**
		 *
		 * Numero de linhas com dados do xls
		 *
		 */
		int linhas = sheet.getRows();

		System.out.println("Quantidade de registros : " + linhas);
		int numeroColuna = sheet.getColumns();

		int numeroColunaMesDia = numeroColuna;

		// Create cell font and format
		WritableFont cellFont = new WritableFont(WritableFont.TAHOMA, 11);
		cellFont.setColour(Colour.BLACK);
		// ajustando formatacao
		WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
		cellFormat.setBackground(Colour.GREEN);
		cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		// ajustando o ajustamento da celula
		CellView cellView = sheet.getColumnView(numeroColuna);
		cellView.setAutosize(true);
		sheet.setColumnView(numeroColuna, cellView);

		Cell coluna_celula = sheet.getCell(2, 2);
		CellFormat format = coluna_celula.getCellFormat();

		Label lable = null;

		lable = new Label(numeroColuna, 0, "Data referência", format);
		sheet.addCell(lable);

		String anoAnterior = "";
		int diasAno = 1;
		for (int i = 1; i < linhas; i++) {

			String mes = sheet.getCell(0, i).getContents();
			String ano = sheet.getCell(1, i).getContents();
			
			//caso o ano tenha mudado, adicionar uma nova coluna
			if (!anoAnterior.equalsIgnoreCase(ano)) {
				anoAnterior = ano;

				lable = new Label(++numeroColuna, 0, "Ano " + ano, cellFormat);
				sheet.addCell(lable);
			}
			//c= 3 posicao onde começa o dia "1" na tabela
			for (int c = 3; c <= 33; c++) {
				//caso o mes seja igual a 1 e o c == 3 : zerar o contador, pois iniciou um novo ano
				if (c == 3 && mes.equalsIgnoreCase("1")) {
					diasAno = 1;
				}

				String valorCelulaDia = sheet.getCell(c, i).getContents();
			
				//adicionando o valor do dia
				lable = new Label(numeroColuna, diasAno++, valorCelulaDia);
				sheet.addCell(lable);
				
				
				//adicionando coluna tabela de referencia
				int diaMes = c;
				lable = new Label(numeroColunaMesDia, (diasAno) - 1, mes + "/" +  ((diaMes) - 2), cellFormat);
				sheet.addCell(lable);
			}

		}

		copiaPlanilha.write();
		copiaPlanilha.close();
		System.out.println("Quantidade de registros processados : " + linhas + " Fim.");
	}

}
